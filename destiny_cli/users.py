__author__ = 'Mike Lloyd'

from json import load, dump

from os.path import isfile

from .destinyerror import DestinyIOError, DestinyError
from .authentication import xbox_userdata
from .config import gamertag, destiny_headers

import requests


def get_membershipid():
    '''
    Gets the membershipID of the given user.
    '''
    if isfile('.destiny/xbox_userdata.json') or isfile('.destiny/psn_userdata.json') is not None:
        try:
            with open('.destiny/xbox_userdata.json') or open('.destiny/psn_userdata.json') as userdata:
                data = load(userdata)
            return data['Response']['user']['membershipId']
        except DestinyIOError as dioe:
            print(dioe.message)
            print("Try to run `destiny --init` again!")
    else:
        xbox_userdata()
        with open('.destiny/xbox_userdata.json') or open('.destiny/psn_userdata.json') as userdata:
                data = load(userdata)
        return data['Response']['user']['membershipId']


def get_destinymembershipid():
    '''
    Gets the destinyMembershipId of the given user.
    '''
    try:
        with open('.destiny/destinyid.json') as userdata:
            data = load(userdata)
        return data['Response'][0]['membershipId']
    except DestinyIOError as dioe:
        print(dioe.message)
        print("Try to run `destiny --init` again!")