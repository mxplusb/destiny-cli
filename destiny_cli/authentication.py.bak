__author__ = 'Mike Lloyd'

import re
from base64 import b64encode

import requests

from urlparse import urlparse
import httplib

httplib.HTTPConnection.debuglevel = 0

from .config import xbox_login, live_pass, live_user, apikey
from .config import psn_login, psn_oauth, psn_username, psn_pass


def xbox_signin():
    exp_urlpost = r'urlPost:\'(https://.*?)\''
    exp_ppft = r'<input type="hidden" name="PPFT" id=".*" value="(.*?)"/>'
    s = requests.Session()

    r = s.get(xbox_login)
    url_post = re.findall(exp_urlpost, r.content.decode())[0]
    ppft = re.findall(exp_ppft, r.content.decode())[0]
    payload = {'login': live_user, 'passwd': live_pass, 'PPFT': ppft}
    r = s.post(url_post, data=payload)

    apiheaders = {'X-API-Key': apikey, 'x-csrf': s.cookies.get_dict()['bungled']}
    # r = s.get('https://www.bungie.net/Platform/Destiny/1/MyAccount/Vault/', headers=apiheaders)
    return apiheaders


def psn_login():
    from logging import getLogger
    logger = getLogger(__name__)

    # Get JSESSIONID cookie.
    # We follow the redirection just in case the URI ever changes.
    get_jessionid = requests.get(psn_login, allow_redirects=True)
    jsessionid0 = get_jessionid.history[1].cookies["JSESSIONID"]
    logger.debug("JSESSIONID: %s", jsessionid0)

    # The POST request will fail if the field `params` isn't present
    # in the body of the request.
    # The value is just the query string of the PSN login page
    # encoded in base64.
    params = urlparse(get_jessionid.url).query
    logger.debug("params: %s", params)
    params64 = b64encode(params)
    logger.debug("params64: %s", params64)

    # Post credentials and pass the JSESSIONID cookie.
    # We get a new JSESSIONID cookie.
    # Note: It doesn't appear to matter what the value of `params` is, but
    # we'll pass in the appropriate value just to be safe.
    post = requests.post(
        psn_oauth,
        data={"j_username": psn_username, "j_password": psn_pass, "params": params64},
        cookies={"JSESSIONID": jsessionid0},
        allow_redirects=False
    )
    if "authentication_error" in post.headers["location"]:
        logger.warning("Invalid credentials")
    jsessionid1 = post.cookies["JSESSIONID"]
    logger.debug("JSESSIONID: %s", jsessionid1)

    # Follow the redirect from the previous request passing in the new
    # JSESSIONID cookie. This gets us the x-np-grant-code to complete
    # the sign-in with Bungie.
    get_grant_code = requests.get(
        post.headers["location"],
        allow_redirects=False,
        cookies={"JSESSIONID": jsessionid1}
    )
    grant_code = get_grant_code.headers["x-np-grant-code"]
    logger.debug("x-np-grant-code: %s", grant_code)

    # Finish our sign-in with Bungie using the grant code.
    auth_cookies = requests.get(psn_login,
                                params={"code": grant_code})

    # Create requests Session.
    session = requests.Session()

    # Save the cookies indicating we've signed in to our session
    session.headers["X-API-Key"] = apikey
    session.headers["x-csrf"] = auth_cookies.cookies["bungled"]
    session.cookies.update({
        "bungleatk": auth_cookies.cookies["bungleatk"],
        "bungled": auth_cookies.cookies["bungled"],
        "bungledid": auth_cookies.cookies["bungledid"]
    })
