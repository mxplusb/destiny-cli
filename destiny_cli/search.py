__author__ = 'Mike Lloyd'

import requests

from .config import gamertag, destiny_headers
from json import dump, load
from os.path import isfile


# TODO mynameismevin I have to change this so it's more ambiguous.
def searchfor_destinymembershipid():
    '''
    Searches for the user's destinyMembershipId.
    '''
    search_url = 'https://www.bungie.net/Platform/Destiny/SearchDestinyPlayer/-1/'
    ending = '/'
    url = search_url + gamertag + ending

    if isfile('.destiny/destinyid.json') is None:
        r = requests.get(url, headers=destiny_headers)

        memberid = r.json()
        with open('.destiny/destinyid.json', 'w+') as data:
            dump(memberid, data)
            data.close()
        with open('.destiny/destinyid.json') as data:
            destinyid = load(data)
    else:
        with open('.destiny/destinyid.json') as data:
            destinyid = load(data)

    return destinyid['Response'][0]['membershipId']
