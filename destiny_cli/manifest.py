__author__ = 'Mike Lloyd'

import os
import zipfile

import requests

from .config import manifest_url


def get_manifest():
    # get the manifest location from the json
    r = requests.get(manifest_url)
    manifest = r.json()
    mani_url = 'http://www.bungie.net' + manifest['Response']['mobileWorldContentPaths']['en']

    # Download the file, write it to 'MANZIP'
    r = requests.get(mani_url)
    with open('.destiny/manifest', "wb") as zippy:
        zippy.write(r.content)

    # Extract the file contents, and rename the extracted file
    # to 'Manifest.content'
    with zipfile.ZipFile('.destiny/manifest') as zippy:
        name = zippy.namelist()
        zippy.extractall()
    os.rename(name[0], '.destiny/manifest.content')
    print('Done')
