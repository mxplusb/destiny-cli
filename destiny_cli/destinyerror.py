__author__ = 'Mike Lloyd'


class DestinyError(BaseException):
    '''
    General error.
    '''

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class DestinyIOError(IOError):
    '''
    General purpose IO Error. Eh.
    '''

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class DestinyInetError(Exception):
    '''
    General purpose connection error.
    '''

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class DestinyJsonError(RuntimeError):
    '''
    General purpose runtime error.
    '''

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class DestinyWarning(Warning):
    '''
    General purpose warning.
    '''

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class DestinyTypeError(TypeError):
    '''
    General purpose type error.
    '''

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)
