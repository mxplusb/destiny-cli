__author__ = 'Mike Lloyd'

# put your API key in here.
apikey = ''

# agnostic gamertag
gamertag = ''

# this is the xbox live login information.
xbox_login = 'https://www.bungie.net/en/User/SignIn/Xuid?bru=%252f'
live_user = ''
live_pass = ''

# TODO will have to test with a PSN account.
# this is the psn login information. untested as I don't have a psn account.
# prolly doesn't work, to be honest as I haven't played with it.
# this is the psn login information. untested as I don't have a psn account.
psn_login = "https://www.bungie.net/en/User/SignIn/Psnid"
psn_oauth = "https://auth.api.sonyentertainmentnetwork.com/login.do"
psn_username = ''
psn_pass = ''

# DO NOT EDIT PAST THIS POINT.
endpoints_url = 'https://www.bungie.net/Scripts/platform.lib.min.js'
manifest_url = 'http://www.bungie.net/Platform/Destiny/Manifest/'
destiny_headers = {"user-agent": "destiny-shoppping", "X-API-Key": apikey}
