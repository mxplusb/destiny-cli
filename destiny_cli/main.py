__author__ = 'mikelloyd'

from .authentication import xbox_userdata
from .destinyerror import DestinyInetError
from .users import get_membershipid, get_destinymembershipid
from .config import gamertag, destiny_headers
from .manifest import get_manifest
from json import dump
from os.path import isfile

import requests


def init():
    from os import mkdir
    from shutil import rmtree
    from os.path import isdir
    # let's make the initial .destiny user data directory.
    if isdir('.destiny') is False:
        mkdir('.destiny')
    else:
        rmtree('.destiny')
        mkdir('.destiny')
    # get the current manifest file.
    get_manifest()
    # search for you gamertag and save the data.
    search_url = 'https://www.bungie.net/Platform/Destiny/SearchDestinyPlayer/-1/'
    ending = '/'
    url = search_url + gamertag + ending
    r = requests.get(url, headers=destiny_headers)
    memberid = r.json()
    with open('.destiny/destinyid.json', 'w+') as data:
            dump(memberid, data)
            data.close()
    try:
        # initialise the local cache of the application.
        xbox_userdata()
        mid = get_membershipid()
        did = get_destinymembershipid()
        print("Your membershipId is %s" % mid)
        print("Your destinyMembershipId is %s" % did)
        # psn stuff goes here when it gets implemeted.
    except DestinyInetError as dinet:
        raise dinet.message


def validate_mid():
    mid = get_membershipid()
    print("Your membershipId is %s" % mid)


def validate_did():
    did = get_destinymembershipid()
    print("Your destinyMembershipId is %s" % did)
