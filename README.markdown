# destiny-cli
Well, you're here. This is the readme for the destiny-cli application. Right now this is a simple tool that pulls
your userdata from Bungie.net so that you can explore the Destiny API. Here soon it will do more then that, but as
I had a super challenging time finding some of this information, I figured I would assist with some of it.

Works on OS X and prolly Linux, but I haven't tested it on Windows. If you need it to work on Windows, let me know
and I can test it on Windows.

Right now it works for Xbox Live accounts, but I don't have a PSN account that I can test with. If anyone in the Denver
area has a Playstation and is willing to let me come over for a few hours to set up a PSN account and play Destiny for
a bit would be super appreciated.

#### Why it's not on PyPI
So right now I am being lazy as hell, and I'm hardcoding all the values into the application so there are immutable
values. What that means is that you can't just search through PyPI and then install it. I guess I could make a source
distribution...hmm. It's on the list.

#### Tools and Helping
I'm using JetBrain's PyCharm for this, so all of my TODOs are throughout the code, so if you want to help, please just
read the [helping](HELP.md) document, where I walk through what is what and how this is built.

#### Usage and Installation
Before you do anything, you need to download this and change a couple values.

In the [config](destiny_cli/config.py) file, you need to change these values with your data:

```python
# put your API key in here.
apikey = ''

# agnostic gamertag
gamertag = ''

# this is the xbox live login information.
xbox_login = 'https://www.bungie.net/en/User/SignIn/Xuid?bru=%252f'
live_user = ''
live_pass = ''

# TODO will have to test with a PSN account.
# this is the psn login information. untested as I don't have a psn account.
# prolly doesn't work, to be honest as I haven't played with it.
psn_login = "https://www.bungie.net/en/User/SignIn/Psnid"
psn_oauth = "https://auth.api.sonyentertainmentnetwork.com/login.do"
psn_username = ''
psn_pass = ''
```

You will be able to change these values, if you mess it up, but you'll have to search through wherever Python installs
them. It's different on every system, so I can't tell specifically. Just be careful and you'll be alright.

After that, do this: `pip install .`

To keep this updated, make sure that you run `git pull` and `git fetch`, followed with a `pip install --upgrade .`. That
will keep things current as I go through and add new features and stuff.

Current Usage:

```bash
OSX-VM:destiny-cli mynameismevin$ destiny --help
usage: destiny [-h] [-v] [--init] [--get-mid] [--get-did]

optional arguments:
  -h, --help      show this help message and exit
  -v, --version   show program's version number and exit
  --init, -i      Initialises the Destiny environment. Will overwrite any data
                  in the local .destiny folder. This creates the local cache of
                  data.
  --get-mid, -gm  Returns your membershipId if you need it for API reference.
  --get-did, -gd  Returns your destinyMembershipId if you need it for API
                  reference.
```
                  
#### Thanks
Contributors and peeps that have assisted, in no particular order.

* FxChip

#### Issues
If you have any issues, just create a Github Issue and I'll address them as I can.